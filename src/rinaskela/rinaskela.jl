module Rinaskela

export Intro, JuliaStandardPkg


include("intro/intro.jl")
include("julia_standard_pkg/julia_standard_pkg.jl")

end
