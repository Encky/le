function increment_int(input_int::Int64)::Int64
    return input_int + 1
end

function increment(input)
    input + 1
end

increment_math_like(x) = x + 1

increment_lambda = x -> x + 1

function example_function(a, b=0, args...; c, d=1, kwargs...)
    return string(a, b, args, c, d, kwargs)
end

f(a, b, c) = a + b + c
