module Intro

export
    add_element_to,
    increment_int,
    increment,
    increment_math_like,
    increment_lambda,
    example_function,
    f

include("example_functions.jl")
include("example_half_useful_function.jl")

end # module
