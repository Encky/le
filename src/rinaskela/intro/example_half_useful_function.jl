function add_element_to(collection::Vector, element)::Array{Any}
    array_to_return = Vector{Any}()
    push!(array_to_return, collection...)
    push!(array_to_return, element)
    return array_to_return
end


function add_element_to(collection::Dict; key, value)::Dict
    dict_to_return = Dict{Any, Any}()
    dict_to_return = merge(dict_to_return, collection)
    dict_to_return[key] = value
    return dict_to_return
end


function add_element_to(collection::Tuple, element)::Tuple
    tuple_to_return = tuple(collection..., element)
    return tuple_to_return
end


function add_element_to(collection::NamedTuple; name, value)::NamedTuple end

