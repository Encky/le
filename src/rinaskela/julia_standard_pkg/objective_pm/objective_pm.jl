module ObjectibePM

const Money = Float64
const Month = Int64

export Money, Time

export
    ProfitAccumulator,
    Staff,
    BusinessParameters

export
    count_money,
    profit,
    lazyness_correction,
    stupidity_correction,
    salary,
    rent_payment,
    equipment_purchase

include("staff.jl")
include("business.jl")

end # module