"""
Структура ProfitAccumulator
"""
mutable struct ProfitAccumulator
    total::Money
    month::Month
    profit_function::Function
    lazyness_correction_function::Function
    stupidity_chance::Float64

    function ProfitAccumulator(;
        profit_function::Function,
        lazyness_correction_function::Function,
        stupidity_chance::Float64,
    )

        @assert stupidity_chance < 1.0 "Chance must be less than 100%"
        @assert stupidity_chance > 0.0 "Chance must be a positive number"

        new(
            Money(0.0),
            Month(1),
            profit_function,
            lazyness_correction_function,
            stupidity_chance,
        )
    end
end


mutable struct Staff
    employees_number::Int64
    salary_mean::Float64
    hire_rate::Function
    rent_payment::Function
    equipment_purchase::Function

    function Staff(
        salary_mean;
        hire_rate::Function,
        rent_payment::Function,
        equipment_purchase::Function,
    )
        new(
            hire_rate(1) |> round |> Int64,
            salary_mean,
            hire_rate,
            rent_payment,
            equipment_purchase,
        )
    end
end


function (profit_acc::ProfitAccumulator)(month::Month, staff::Staff)
    profit_acc.month = month
    profit_acc.total +=
        (staff = staff, profit_acc = profit_acc) |>
        profit |>
        lazyness_correction |>
        stupidity_correction |>
        first
end

function profit(
    tuple::NamedTuple{(:staff, :profit_acc), Tuple{Staff, ProfitAccumulator}}
)::NamedTuple
    
    return (
        money = tuple.profit_acc.month |> tuple.profit_acc.profit_function,
        staff = tuple.staff,
        profit_acc = tuple.profit_acc
    )
end

function lazyness_correction(
    tuple::NamedTuple{(:money, :staff, :profit_acc), Tuple{Money, Staff, ProfitAccumulator}}
)::NamedTuple

    return (
        money = tuple.money *
            tuple.profit_acc.lazyness_correction_function(tuple.staff.employees_number),
        staff = tuple.staff,
        profit_acc = tuple.profit_acc
    )
end

function stupidity_correction(
    tuple::NamedTuple{(:money, :staff, :profit_acc), Tuple{Money, Staff, ProfitAccumulator}}
)::NamedTuple

    corr = rand() < tuple.profit_acc.stupidity_chance ? one(Money) : zero(Money)
    tuple.staff.employees_number -= 1
    return (
        money = tuple.money - corr,
        staff = tuple.staff,
        profit_acc = tuple.profit_acc
    )
end

function (staff::Staff)(month::Month)
    staff.employees_number = month |> staff.hire_rate |> round |> Int64
end

function salary(staff::Staff, profit_acc::ProfitAccumulator, month::Month)
    profit_acc.total -= staff.employees_number * staff.salary_mean
end

function rent_payment(staff::Staff, profit_acc::ProfitAccumulator, month::Month)
    profit_acc.total -= staff.rent_payment(month)
end

function equipment_purchase(staff::Staff, profit_acc::ProfitAccumulator, month::Month)
    profit_acc.total -= staff.equipment_purchase(month)
end
