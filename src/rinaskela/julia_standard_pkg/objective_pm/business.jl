"""
Задача:

Каждый месяц в течение 20ти лет компания наслаждается линейно растущим доходом по закону
20 + 0.3t, где t это номер месяца.
Каждый месяц нужно платить сотрудникам по 0,1. Количество сотрудников растёт
по закону 100*ln(t+1.1).
Неэффективность растёт с ростом сотрудников по закону 100-10*ln(n+1.1).
С вероятностью 2% могут нанять глупого сотрудника, который принесёт в 1 млн в первый месяц
и его сразу уволят.
На покупку оборудования уходит 2 млн в год.
На аренду уходит 10-0.2t в месяц, сотрудники уходят на удалёнку.
С вероятностью 10% в год после первого года, надо платить откат в 1 млн. В первый месяц
его нужно оплатить с пероятностью 100%.

Как много денег будет у компании через 20 лет?
"""
mutable struct BusinessParameters
    bribe_chance::Float64
    staff::Staff
    profit_accumulator::ProfitAccumulator

    function BusinessParameters(;
        bribe_chance::Float64,
        staff::Staff,
        profit_accumulator::ProfitAccumulator
    )
        new(bribe_chance, staff, profit_accumulator)
    end
end


function count_money(bp::BusinessParameters, time_span::Int64)
    months = [Month(x) for x = 1:time_span]
    for month in months
        bp.profit_accumulator(month, bp.staff)
        bp.staff(month)
        compact_args = (bp.staff, bp.profit_accumulator, month)
        salary(compact_args...)
        rent_payment(compact_args...)
        equipment_purchase(compact_args...)
        if month > 12 && rand() < bp.bribe_chance
            bp.profit_accumulator.total -= 1
        end
    end

    @info bp.profit_accumulator.total
end


count_money(
    BusinessParameters(;
        bribe_chance = 0.1/12,
        staff = Staff(
            0.1;
            hire_rate = x -> 100*log(x+1.1),
            rent_payment = x -> 10-0.2*x,
            equipment_purchase = x -> 2.0/12,
        ),
        profit_accumulator = ProfitAccumulator(;
            profit_function = x -> 20 + 0.3*x,
            lazyness_correction_function = x -> 100-10*log(x+1.1),
            stupidity_chance = 0.02,
        ),
    ),
    20*12,
)
