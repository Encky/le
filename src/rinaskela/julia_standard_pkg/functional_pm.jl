"""
Задача:

Каждый месяц в течение 20ти лет компания наслаждается линейно растущим доходом по закону
20 + 0.3t, где t это номер месяца.
Каждый месяц нужно платить сотрудникам по 0,1. Количество сотрудников растёт
по закону 100*ln(t+1.1).
Неэффективность растёт с ростом сотрудников по закону 100-10*ln(n+1.1).
С вероятностью 2% могут нанять глупого сотрудника, который принесёт в 1 млн в первый месяц
и его сразу уволят.
На покупку оборудования уходит 2 млн в год.
На аренду уходит 10-0.2t в месяц, сотрудники уходят на удалёнку.
С вероятностью 10% в год после первого года, надо платить откат в 1 млн. В первый месяц
его нужно оплатить с пероятностью 100%.

Как много денег будет у компании через 20 лет?
"""
const Money = Float64


function correct_money(last_total::Money, percent)::Money
    if percent < 0
        throw(ArgumentError("Percent must be positive"))
    end
    
    return last_total * percent
end


function accumulate_money(last_total::Money, amount)::Money
    return last_total + amount
end


staff_count(month) = round(100*log(month + 1.1))


total = Money(-1)
staff_incident = 0
for month = 1:24*12
    global staff_incident
    global total
    staff = staff_count(month) - staff_incident
    after_income = accumulate_money(total, 20+0.3*month)
    after_salary = accumulate_money(after_income, -0.1*staff)
    after_laziness = correct_money(after_salary, 0.01*(100-10*log(staff+1.1)))
    if rand() < 0.02
        staff_incident += 1
        after_stupidity = accumulate_money(after_laziness, -1)
    else
        after_stupidity = after_laziness
    end
    after_equipment = accumulate_money(after_stupidity, -2/12)
    after_rent = accumulate_money(after_equipment, -10-0.2*month)
    if round(month/12) > 1 && rand() < 0.1/12
        after_bribe = accumulate_money(after_rent, -1)
    else
        after_bribe = after_rent
    end
    total = after_bribe
end

println(total)