module JuliaStandardPkg

export Money, Time

export
    ProfitAccumulator,
    Staff,
    BusinessParameters

export
    correct_money,
    accumulate_money,
    count_money,
    profit,
    lazyness_correction,
    stupidity_correction,
    salary,
    rent_payment,
    equipment_purchase

include("procedure_pm.jl")
include("functional_pm.jl")
include("objective_pm/objective_pm.jl")

end # module