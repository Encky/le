mutable struct MyObject{T} <: Le3_objects 
    money::T

    function MyObject()
        new(1)
    end
end

(c::MyObject{T1})(m::money, s::T2) where {T1, T2} = c.money
