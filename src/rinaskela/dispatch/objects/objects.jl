include("my_object_none.jl")
include()
include()

mutable struct MyObject{T} <: Le3_objects end

MyObject{:Two}(money::Money, staff::Staff) = 
    MyObject(money, staff)

MyObject{:One}(money::Money) = 
    MyObject(money)
    
MyObject{:None}() = MyObject()
