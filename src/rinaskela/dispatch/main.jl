function init()
    u0 = zeros(Float64, 3)

    objects = Le3Objects[]

    push!(objects, Le3Objects.MyObject{:None}())
    u[1] = objects[1](0)

    push!(objects, Le3Objects.MyObject{:One}(20))
    u[2] = objects[2](0)

    push!(objects, Le3Objects.MyObject{:Two}(20, 1000))
    u[3] = objects[3](0)
end

function calc(t)
    u[1] = objects[1](t)

    u[2] = objects[2](t)

    u[3] = objects[3](t)

    return nothing
end