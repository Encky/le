module Dispatch

import JuliaStandardPkg.ObjectibePM: Time, Money

abstract type Le3Objects end

mutable struct Main
    t::Time

end

include("interfaces.jl")
include("main.jl")
include("objects/objects.jl")

end