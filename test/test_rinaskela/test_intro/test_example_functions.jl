import le.Rinaskela.Intro.increment_int
import le.Rinaskela.Intro.increment
import le.Rinaskela.Intro.increment_math_like
import le.Rinaskela.Intro.increment_lambda
import le.Rinaskela.Intro.example_function
import le.Rinaskela.Intro.f

@testset "Example functions" begin
    @testset "increment_int with right argument(s)" begin
        @test increment_int(1) == 2
    end

    @testset "increment with right argument(s)" begin
        @test increment(1.0) == 2.0
        @test increment(1) == 2
    end

    @testset "increment_math_like with right argument(s)" begin
        @test increment_math_like(1.0) == 2.0
        @test increment_math_like(1) == 2
    end

    @testset "increment_lambda with right argument(s)" begin
        @test increment_lambda(1.0) == 2.0
        @test increment_lambda(1) == 2
    end

    @testset "example_function with right argument(s)" begin
        @test example_function('a', 2, 3, 4; c=3, e=7) == "a2(3, 4)31Base.Pairs(:e => 7)"
    end

    @testset "f with right argument(s)" begin
        @test f(1, 2, 3) == 6
    end
end