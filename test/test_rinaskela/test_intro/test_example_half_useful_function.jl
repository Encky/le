import le.Rinaskela.Intro.add_element_to

@testset "Half-useful function test" begin
    @testset "Vector input" begin
        vector_input = [1, 2, 3]
        vector_output = [1, 2, 3, "4"]
        @test add_element_to(vector_input, "4") == vector_output
    end

    @testset "Dict input" begin
        dict_input = Dict(1 => "one", 2 => "two", 3 => "three")    
        dict_output = Dict(1 => "one", 2 => "two", 3 => "three", "four" => 4)
        @test add_element_to(dict_input, key="four", value=4) == dict_output
    end

    @testset "Tuple input" begin
        tuple_input = (0.0, "hello", 6*7)
        tuple_output = (0.0, "hello", 6*7, 55555)
        @test add_element_to(tuple_input, 55555) == tuple_output

        tuple_input = (0, 1, 2)
        tuple_output = (0, 1, 2, "five")
        @test add_element_to(tuple_input, "five") == tuple_output
    end
end