import le.Rinaskela.JuliaStandardPkg.ObjectibePM.ProfitAccumulator
import le.Rinaskela.JuliaStandardPkg.ObjectibePM.Month
import le.Rinaskela.JuliaStandardPkg.ObjectibePM.Money


@testset verbose = true "Profit Accumulator" begin
    @testset verbose = true "Creation Profit Accumulator with right arguments" begin
        _p_func = x -> 20 + 0.3*x
        _l_func = x -> 100-10*log(x+1.1)
        pa = ProfitAccumulator(;
            profit_function = _p_func,
            lazyness_correction_function = _l_func,
            stupidity_chance = 0.02,
        )

        @test pa.total == zero(Float64)
        @test pa.total isa Money
        @test pa.month == one(Int64)
        @test pa.month isa Month
        @test pa.profit_function == _p_func
        @test pa.lazyness_correction_function == _l_func
        @test pa.stupidity_chance == 0.02
    end

    @testset verbose = true "Creation Profit Accumulator with wrong arguments" begin
        expected_message = "Chance must be less than 100%"
        @test_throws AssertionError(expected_message) begin
            ProfitAccumulator(;
                profit_function = x -> x,
                lazyness_correction_function = x -> x,
                stupidity_chance = 1.02,
            )
        end

        expected_message = "Chance must be a positive number"
        @test_throws AssertionError(expected_message) begin
            ProfitAccumulator(;
                profit_function = x -> x,
                lazyness_correction_function = x -> x,
                stupidity_chance = -1.02,
            )
        end
    end
end